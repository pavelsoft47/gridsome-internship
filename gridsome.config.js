module.exports = {
  siteName: 'Kozak Group Magento Internship',
  siteDescription: 'Take an internship in magento with the Kozak Group',
  plugins: [
    {
      use: '@gridsome/source-strapi',
      options: {
        apiURL: process.env.GRIDSOME_API_URL,
        queryLimit: 100,
        singleTypes: [`landing-page`],
      },
    },
    {
      use: 'gridsome-plugin-vue-toasted',
      options:  {
        position: 'top-center',
        singleton: true,
        duration: 5000,
      }
    }
  ]
}
